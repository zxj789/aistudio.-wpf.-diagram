﻿using System.Xml.Serialization;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.ViewModels;

namespace AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.Models
{
    public class PersistDesignerItem : DesignerItemBase
    {       
        public PersistDesignerItem()
        {

        }
        public PersistDesignerItem(PersistDesignerItemViewModel item) : base(item)
        {
            this.HostUrl = item.HostUrl;
        }

        [XmlAttribute]
        public string HostUrl { get; set; }
    }
}
