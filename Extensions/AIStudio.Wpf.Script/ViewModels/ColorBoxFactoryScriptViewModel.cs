﻿using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using AIStudio.Wpf.Script.Core.Models;
using AIStudio.Wpf.Script.Core.ViewModels;
using System.Collections.Generic;

namespace AIStudio.Wpf.Script.ViewModels
{
    public class ColorBoxFactoryScriptViewModel : RoslynScriptViewModel
    {
        public ColorBoxFactoryScriptViewModel()
        {
        }

        public ColorBoxFactoryScriptViewModel(IDiagramViewModel root) : base(root)
        {
        }

        public ColorBoxFactoryScriptViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public ColorBoxFactoryScriptViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {
        }

        protected override void InitNew()
        {
            base.InitNew();

            ItemWidth = 230;
            ItemHeight = 230;
            Code = @"using System;
using System.Collections.Generic;
using AIStudio.Wpf.Script.Core.Models;
using System.Windows.Media;
using System.Linq;

namespace AIStudio.Wpf.CSharpScript
{
    public class ColorBoxFactory
    {   
        public int Count{private get;set;} = 100;
        private List<ColorBoxModel> ItemsSource { get; set;} = new List<ColorBoxModel>();
        public ColorBoxModel Output {get;private set;}

        public void Execute()
        {
        	if (ItemsSource.Count == 0)
            {
                Random random = new Random();

                for (int i = 0; i < Count; i++)
                {
                    var sharpindex = random.Next(0, 8);
                    if (sharpindex == 7)
                    {
                        sharpindex = 0;
                    }

                    var colorindex = random.Next(0, 8);
                    if (colorindex == 7)
                    {
                        colorindex = 0;
                    }
                    string sharp = ColorBoxModel.SettingSharps[sharpindex];
                    Color color = ColorBoxModel.SettingColors[colorindex];

                    ColorBoxModel colorBoxModel = new ColorBoxModel(i.ToString(), sharp, color);
                    ItemsSource.Add(colorBoxModel);
                }
                Console.WriteLine($""初始化完成,一共初始化Box{Count}个"");
            }            
            else if (Output == null)
            {
                Output = ItemsSource.FirstOrDefault();
                ItemsSource.RemoveAt(0);
                Console.WriteLine($""装配{Output.Text}号Box"");
            }
        }
    } 
}";
        }

        private List<ColorBoxModel> _itemsSource;
        public List<ColorBoxModel> ItemsSource
        {
            get
            {
                return _itemsSource;
            }
            set
            {
                SetProperty(ref _itemsSource, value);
            }
        }

      
    } 

 
}
