﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIStudio.Wpf.Mind.Controls
{
    /// <summary>
    /// LinkControl.xaml 的交互逻辑
    /// </summary>
    [TemplatePart(Name = "PART_InnerHyperlink", Type = typeof(Hyperlink))]
    public class LinkControl : Control
    {
        public static readonly DependencyProperty UrlProperty =
            DependencyProperty.Register(nameof(Url), typeof(string), typeof(LinkControl));

        [Category("Common Properties"), Bindable(true)]
        public string Url
        {
            get
            {
                return GetValue(UrlProperty) as string;
            }
            set
            {
                SetValue(UrlProperty, value);
            }
        }

        static LinkControl()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(LinkControl),
                new FrameworkPropertyMetadata(typeof(LinkControl)));

        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Hyperlink innerHyperlink = GetTemplateChild("PART_InnerHyperlink") as Hyperlink;
            if (innerHyperlink != null)
            {
                innerHyperlink.Click += new RoutedEventHandler(InnerHyperlink_Click);
            }
        }

        void InnerHyperlink_Click(object sender, RoutedEventArgs e)
        {
            // 激活的是当前默认的浏览器
            Process.Start("explorer.exe", Url);
        }
    }
}
