﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using AIStudio.Wpf.Mind.Helpers;
using AIStudio.Wpf.Mind.Models;

namespace AIStudio.Wpf.Mind.Converters
{
    public class MindThemeFillBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is MindTheme mindTheme)
            {
                var mindThemeModel = MindThemeHelper.GetTheme(mindTheme);
                return new SolidColorBrush(mindThemeModel.MindThemeLevel0.FillColor);
            }

            return new SolidColorBrush();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
