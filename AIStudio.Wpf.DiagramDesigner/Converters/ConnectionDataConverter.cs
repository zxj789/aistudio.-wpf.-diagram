﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ConnectionDataConverter : IMultiValueConverter
    {
        static ConnectionDataConverter()
        {
            Instance = new ConnectionDataConverter();
        }

        public static ConnectionDataConverter Instance
        {
            get;
            private set;
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            PathGeometry pathGeometry = new PathGeometry();
            if (values[0] != null)
            {
                List<ConnectorPointModel> points = (List<ConnectorPointModel>)values[0];

                PathFigure figure = new PathFigure();
                figure.StartPoint = (PointBase)points[0];          
                for (int i = 0; i < points.Count; i++)
                {
                    LineSegment arc = new LineSegment((PointBase)points[i], true);
                    figure.Segments.Add(arc);
                }              
                pathGeometry.Figures.Add(figure);
            }
            return pathGeometry;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
