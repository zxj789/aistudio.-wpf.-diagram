﻿using System;
using System.Linq;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public static partial class PathGenerators
    {
        public static double SourceMarkerAdjustement(PointBase[] route, double markerWidth, double markerHeight)
        {
            var angleInRadians = Math.Atan2(route[1].Y - route[0].Y, route[1].X - route[0].X) + Math.PI;
            var xChange = markerWidth * Math.Cos(angleInRadians);
            var yChange = markerHeight * Math.Sin(angleInRadians);
            route[0] = new PointBase(route[0].X - xChange, route[0].Y - yChange);
            return angleInRadians * 180 / Math.PI;
        }

        public static double TargetMarkerAdjustement(PointBase[] route, double markerWidth, double markerHeight)
        {
            var angleInRadians = Math.Atan2(route[route.Length - 1].Y - route[route.Length - 2].Y, route[route.Length - 1].X - route[route.Length - 2].X);
            var xChange = markerWidth * Math.Cos(angleInRadians);
            var yChange = markerHeight * Math.Sin(angleInRadians);
            route[route.Length - 1] = new PointBase(route[route.Length - 1].X - xChange, route[route.Length - 1].Y - yChange);
            return angleInRadians * 180 / Math.PI;
        }

        public static PointBase[] ConcatRouteAndSourceAndTarget(PointBase[] route, PointBase source, PointBase target)
        {
            var result = new PointBase[route.Length + 2];
            result[0] = source;
            route.CopyTo(result, 1);
            result[result.Length - 1] = target;
            return result;
        }

        private static void DoShift(PointBase[] points, ConnectionViewModel link)
        {
            double left = link.Area.Left;
            double top = link.Area.Top;

            for (int i = 0; i < points.Length; i++)
            {
                points[i].X = points[i].X - left;
                points[i].Y = points[i].Y - top;
            }
        }
    }
}
