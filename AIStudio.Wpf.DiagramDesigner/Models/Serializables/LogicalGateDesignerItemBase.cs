﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class LogicalGateDesignerItemBase : DesignerItemBase
    {
        public LogicalGateDesignerItemBase()
        {

        }
        public LogicalGateDesignerItemBase(LogicalGateItemViewModelBase item, string reserve = null) : base(item, reserve)
        {
            this.OrderNumber = item.OrderNumber;
            this.LogicalType = item.LogicalType;
            this.Value = item.Value;
            this.IsEnabled = item.IsEnabled;
            LogicalConnectors = new List<LogicalConnectorInfoItem>(item.Connectors.OfType<LogicalConnectorInfo>().Select(p => new LogicalConnectorInfoItem(p)));
        }

        [XmlArray]
        public List<LogicalConnectorInfoItem> LogicalConnectors
        {
            get; set;
        }

        [XmlAttribute]
        public int OrderNumber { get; set; }

        [XmlAttribute]
        public double Value { get; set; }

        [XmlAttribute]
        public LogicalType LogicalType { get; set; }

        [XmlAttribute]
        public bool IsEnabled { get; set; }

    }
}
